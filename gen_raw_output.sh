#!/bin/bash
ticket_back=$(pwd)
cd /home/fopor/Documents/Dev/ompcbench
source venv/bin/activate
cd $ticket_back

for node in "04" "08" "16" "32" "64"
do
    for size in "0_25" "0_50" "1_0" "2_0" "4_0" "8_0" "16_0" "32_0" "64_0"
    do
        #output_file="$size""_GB.txt"
        output_file="$node""w.txt"
        exp_name="gen_experiment_""$size""GB_""$node""w_000"
        ompcbench analyze $exp_name | head -16 | tail -8 >> $output_file
    done
done
