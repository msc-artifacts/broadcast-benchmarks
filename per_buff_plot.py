#!/usr/bin/env python3

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import argparse
import re

parser = argparse.ArgumentParser()
parser.add_argument('input_file', type=str)
args = parser.parse_args()

sns.set()
sns.set_theme(style="whitegrid")

bcast_data_size = []
n_nodes = 0

d = {}

with open(args.input_file) as f:
    dep_type = f.readline()
    n_nodes = int(f.readline())
    bcast_data_size = [float(x) for x in f.readline().split(' ')]

    # remainder of file is benchmark types
    for bench in f.readlines():
        bench = bench.lstrip()
        bench = re.sub(' +', ' ', bench)
        bench = re.sub(r"\s+$", "", bench)  # remove trailing space
        data = bench.split(' ')
        d[data[0]] = [float(x) for x in data[1:]]

# convert bytes to MB
bcast_data_size = [int(x / 10**6) for x in bcast_data_size]

ax = plt.axes()

for bench_label, bench_data in d.items():
    elapsed_time = bench_data[0::2]
    et_err = bench_data[1::2]
    if 'packing' in bench_label:
        plt.errorbar(bcast_data_size, elapsed_time, linestyle='dashed',
                     label=bench_label, marker='o', yerr=et_err)
    else:
        plt.errorbar(bcast_data_size, elapsed_time, linestyle='dotted',
                     label=bench_label, marker='s', yerr=et_err)

ax.set_xscale("log")
ax.set_xticks(bcast_data_size)
ax.get_xaxis().set_major_formatter(ticker.ScalarFormatter())
ax.get_xaxis().set_minor_formatter('')

plt.subplots_adjust(top=0.9)

plt.suptitle(
    f'{n_nodes} worker nodes\nTask Bench dependency type: {dep_type}\n\n', y=0.99)
plt.ylabel('time (seconds)')
plt.xlabel('Broadcast buffer size (megabytes)')
plt.savefig(f'graphs/per_buff_graph_{n_nodes}_no_legend.pdf', bbox_inches='tight')
lgd = plt.legend(title="Broadcast \nevent type",
                 loc='center left', bbox_to_anchor=(1, 0.5))

plt.savefig(f'graphs/per_buff_graph_{n_nodes}.pdf', bbox_inches='tight')
