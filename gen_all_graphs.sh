# per node
files=$(ls ompcbench-parser/parsed_summary | grep per_node)
for i in $files; do
    echo "Plotting $i"
    python3 per_node_plot.py ompcbench-parser/parsed_summary/$i
done

# per buff
files=$(ls ompcbench-parser/parsed_summary | grep per_buff)
for i in $files; do
    echo "Plotting $i"
    python3 per_buff_plot.py ompcbench-parser/parsed_summary/$i
done
