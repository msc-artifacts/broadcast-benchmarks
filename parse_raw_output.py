#!/usr/bin/env python3

import re

#warn = False
warn = True
warnings = []

parsed = {
        'dynbcast': {},
        'mpibcast': {},
        'nobcast': {},
        'p2p': {},
        }

sizes = ['0_25', '0_50', '1_0', '2_0', '4_0', '8_0', '16_0', '32_0', '64_0']

# append space to avoid matching both packed version
strategies = [f"{el} " for el in list(parsed.keys())]

for size in sizes:
    filename = f"raw_output/{size}_GB.txt"
    with open(filename) as f:
        content = list(f.readlines())
        workers = -1
        buff_size = int(float(size.replace('_', '.')) * 1e3)

        for idx, line in enumerate(content):
            if 'compute_bound' in line:
                workers = re.sub(r'\s+', ' ', line)
                workers = int(workers.split(' ')[1]) - 1

            # check if line is about any strategy
            res = [ele for ele in strategies if(ele in line)]
            if len(res) > 1:
                warnings.append("ERROR: double match in result line")
                exit()

            if len(res) == 1:
                strategy = res[0].rstrip()

                if f"{strategy} " in line:
                    data = re.search(r'\d+\.\d+\s\d+\.\d+', line)
                    if data:
                        time, err = data.group().split(' ')
                        parsed[strategy][(workers, buff_size)] = (time, err)
                    else:
                        # has value but no std err
                        data = re.search(r'\d+\.\d+\s+\-', line)
                        if data:
                            time = data.group().split(' ')[0]
                            parsed[strategy][(workers, buff_size)] = (time, 0)
                            warnings.append(f"WARNING: missing error field for"
                                 f" {buff_size}MB for {workers} workers using "
                                 f"{strategy}")


# sumarize meta-values
parsed_sizes = []
parsed_workers = []
for item in parsed.values():
    for el in item:
        worker, size = el
        if worker not in parsed_workers:
            parsed_workers.append(worker)

        if size not in parsed_sizes:
            parsed_sizes.append(size)

parsed_sizes.sort()
parsed_workers.sort()

# generate per_node summary
for size in parsed_sizes:
    output_text = []
    output_text.append("stencil_1d\n")
    output_text.append(str(size) + '\n')
    output_text.append(' '.join([str(it) for it in parsed_workers]) + '\n')
    for strategy in strategies:
        strategy = strategy.rstrip()
        output_text.append(strategy + ' ')
        for worker in parsed_workers:
            if strategy in parsed:
                if (worker, size) in parsed[strategy]:
                    time, err = parsed[strategy][(worker, size)]
                else:
                    warnings.append((f"WARNING: for size {size:5}MB with "
                          f"{worker:2} workers missing strategy {strategy}"))
                    time = 0
                    err = 0

                output_text.append(f"{time} {err}" + " ")
            else:
                warnings.append((f"WARNING: missing strategy {strategy}"))

        output_text.append("\n")

    output_filename = f"per_node_input_{size}MB.txt"
    output_content=''.join(output_text)
    with open(f"parsed_summary/{output_filename}", "w") as of:
        of.write(output_content)


# generate per_buff summary
for worker in parsed_workers:
    output_text = []
    output_text.append("stencil_1d\n")
    output_text.append(str(worker) + '\n')
    output_text.append(' '.join([str(int(it*1e6)) for it in parsed_sizes]) + '\n')
    for strategy in strategies:
        strategy = strategy.rstrip()
        output_text.append(strategy + ' ')
        for size in parsed_sizes:
            if strategy in parsed:
                if (worker, size) in parsed[strategy]:
                    time, err = parsed[strategy][(worker, size)]
                else:
                    warnings.append((f"WARNING: for size {size:5}MB with "
                          f"{worker:2} workers missing strategy {strategy}"))
                    time = 0
                    err = 0

                output_text.append(f"{time} {err}" + " ")
            else:
                warnings.append((f"WARNING: missing strategy {strategy}"))
        output_text.append("\n")

    output_filename = f"per_buff_input_{worker}w.txt"
    output_content =''.join(output_text)
    with open(f"parsed_summary/{output_filename}", "w") as of:
        of.write(output_content)


warnings=set(warnings) #remove duplicates
print(f"Generated {len(warnings)} warnings!")
if warn:
    for warning in sorted(warnings):
        print(warning)

