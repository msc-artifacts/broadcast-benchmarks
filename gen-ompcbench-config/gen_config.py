from jinja2 import Template, Environment, FileSystemLoader, StrictUndefined
from tqdm import tqdm

strats = [
    "dynbcast",
    "dynbcast_packing",
    "nobcast",
    "nobcast_packing",
    "p2p",
    "p2p_packing",
    "mpibcast",
    "mpibcast_packing",
]

deps = [
    "stencil_1d",
]

bd = {
    'strats': strats,
    'deps': deps,
}

with open('benchmark_template') as f:
    global template
    template = Template(f.read())

# matrix combination
bcast_buffer_sizes = [
    0.25e9,  # 250MB
    0.50e9,
    1.00e9,  # 1GB
    2.00e9,
    4.00e9,
    8.00e9,
    16.00e9,
    32.00e9,
    64.00e9,
]

worker_node_sizes = [
    4,
    8,
    16,
    32,
    64,
]


for n_workers in tqdm(worker_node_sizes):
    bd['workers_str'] = ("%02.0f" % n_workers)
    bd['n_workers'] = n_workers
    for buffsize in bcast_buffer_sizes:
        if buffsize/1e9 < 1:
            bd['buff_str'] = ("%.2f" % (buffsize/1e9)).replace('.', '_')
        else:
            bd['buff_str'] = ("%.1f" % (buffsize/1e9)).replace('.', '_')

        bd['bcast_buffer'] = ("%0.0f" % buffsize)

        output_name = f"gen_{bd['buff_str']}GB_{bd['workers_str']}workers.yaml"
        view = template.render(bd, undefined=StrictUndefined)

        # save content
        with open(f"./bench-yamls/{output_name}", "w") as fh:
            fh.write(view)
